const hp = require('http').createServer();
const io = require('socket.io')(hp,{
    cors: { origin: "*" }
});

io.on('connection', (socket) => {
    console.log('User Connected!');

    socket.on('message', (message) => {
        console.log(message)

        io.emit('message', `${socket.id.substr(0,2)} said ${message}` );
    });
}); 

hp.listen(8080, () => {
    console.log(`Server is running on port ${8080}`);
});
