# Final Lab Submission

### Submitted by: Sunil Prajapati
### ID: 124073

## PART A
It includes the authentication using Nodejs with express and mongodb

To start the server enter the part A directory

First install all the packages:

`yarn install`

To run the server, run the command:

`node app.js`


The server will be available at http://localhost:3000

## Part B
It include the websockets part

Go the directory `/part-B/server`

First install all the packages:

`yarn install`

To run the server, run the command:

`node index.js`

The server will be available at http://localhost:8080


