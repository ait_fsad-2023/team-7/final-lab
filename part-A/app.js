const express = require('express');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser')

const app = express();

// middleware
app.use(express.static('public'));

// view engine
app.set('view engine', 'ejs');

app.use(express.json())
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser())

// routes
app.get('/', (req, res) => res.render('home'));
app.get('/smoothies', (req, res) => res.render('smoothies'));

const authRoutes = require('./routes/auth');
app.use('/auth', authRoutes)

// database connection
const dbURI = 'mongodb://localhost/final-lab';
mongoose.connect(dbURI, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex:true })
  .then((result) => {
    console.log('Connection to DB successful');
    console.log('The server is running at http://localhost:3000')
    app.listen(3000)
  })
  .catch((err) => console.log(err));