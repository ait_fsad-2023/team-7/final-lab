const User = require('../models/user')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

const maxAge = 3 * 24 * 60 * 60; 

const createToken = (id) => {
    return jwt.sign({ id }, 'jwt-secret', { expiresIn: maxAge })
}

const login_get = (req, res) => {
    res.render('login')
}

const login_post = async (req, res) => {
    const { email, password } = req.body

    try {
        const user = await User.findOne({
            email: email
        })

        if (user) {
            const salt = await bcrypt.genSalt()

            const hashedPassword = await bcrypt.hash(password, salt)
        
            if (bcrypt.compare(hashedPassword, user.password)) {
                const token = await createToken(user._id)

                res.cookie('jwt', token, { httpOnly: true, maxAge: maxAge * 1000 }); 

                res.status(201).json(user)
            } else {
                res.status(404).json({ 'error': 'Invalid Password'})
            }
        } else {
            res.status(404).json({ 'error': 'Email not found'})
        }
        
    } catch (err) {
        res.status(400).json({ err })
    }
}

module.exports = {
    login_post,
    login_get
}
